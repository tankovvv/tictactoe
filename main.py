import random

board = ['1', '2', '3', '4', '5', '6', '7', '8', '9']
chsvar = [0, 1, 2, 3, 4, 5, 6, 7, 8]

game_running = True
counter = 0


def pt():
    global counter

    temp = int(input('Игрок X, выбери клетку: ')) - 1
    if board[temp].isdigit():
        board[temp] = 'X'
        chsvar.remove(temp)
    else:
        print('[Ошибка] Данная клетка уже кем-то занята, выбери свободную.')
        counter -= 1


def ct():
    temp = random.choice(chsvar)
    print(f'Компьютер выбрал клетку: {temp + 1}')
    board[temp] = 'O'
    chsvar.remove(temp)


def pr_board():
    print(board[0], board[1], board[2])
    print(board[3], board[4], board[5])
    print(board[6], board[7], board[8])


def endchecker():
    global game_running
    for i in range(3):
        if board[0 + i * 3] == board[1 + i * 3] == board[2 + i * 3]:  # По горизонтали
            game_running = False
            print('\nИгра окончена!\n\nПобедил игрок', board[0 + i * 3])
            pr_board()
        elif i < 3:
            if board[0 + i] == board[3 + i] == board[6 + i]:  # По вертикали
                game_running = False
                print('\nИгра окончена!\n\nПобедил игрок', board[0 + i])
                pr_board()
        if board[0] == board[4] == board[8]:  # По горизонтали
            game_running = False
            print('\nИгра окончена!\n\nПобедил игрок', board[0])
            pr_board()
        elif board[6] == board[4] == board[2]:
            game_running = False
            print('\nИгра окончена!\n\nПобедил игрок', board[6])
            pr_board()
        elif counter == 9:
            game_running = False
            print('\nИгра окончена!\n\nНичья!')
            pr_board()


chs = input('Добро пожаловать в крестики-нолики! Выбери кто будет ходить первым: \n1 - Ты\n2 - Компьюетр\nВыбор: ')
while game_running and counter <= 9:
    pr_board()
    counter += 1
    if counter % 2 != 0:
        if chs == '1':
            pt()
        else:
            ct()
    else:
        if chs == '1':
            ct()
        else:
            pt()
    endchecker()

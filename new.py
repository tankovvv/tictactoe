import copy
class Board:
    def __init__(self, board, valid_move):
        self.board = board
        self.valid_move = valid_move
        self.side = 'x'
        self.winner = None
    def check_win(self):
        for i in range(3):
            if self.board[0 + i * 3] == self.board[1 + i * 3] == self.board[2 + i * 3]:  # По горизонтали
                self.winner = self.board[1 + i * 3]
                break
            elif self.board[0 + i] == self.board[3 + i] == self.board[6 + i]:  # По вертикали
                self.winner = self.board[3 + i]
                break
            elif self.board[0] == self.board[4] == self.board[8]:  # По горизонтали
                self.winner = self.board[4]
                break
            elif self.board[6] == self.board[4] == self.board[2]:
                self.winner = self.board[4]
                break
    def showboard(self):
        print(self.board[0],self.board[1],self.board[2])
        print(self.board[3],self.board[4],self.board[5])
        print(self.board[6],self.board[7],self.board[8])
class Node:
    def __init__(self, board, win_x = 0, win_0 = 0, draw = 0, move = None, parents = None):
        self.board = board
        self.win_x = win_x
        self.win_0 = win_0
        self.draw = draw
        self.move = move
        self.parents = parents
        self.child = []
class Tree:
    def __init__(self, root):
        self.root = root
    def generate_tree(self, node):
        print(f'''
         {self.root.win_x}
         {self.root.win_0}
         {self.root.draw}''')
        for i in node.board.valid_move:
            a = copy.deepcopy(node.board)
            n = Node(a, 0, 0, 0, parents = node)
            node.child.append(n)
            n.board.board[i] = n.board.side
            if n.board.side == 'x':
                n.board.side = '0'
            else:
                n.board.side = 'x'
            n.move = i
            n.board.check_win()
            if n.board.winner != None:
                self.improve_grade(n, n.board.winner)
                continue
            elif len(node.board.valid_move) == 1:
                self.improve_draw(n)
            n.board.valid_move.remove(i)
            self.generate_tree(n)
                
                
    def improve_grade(self, node, winner):
        if winner == 'x':
            node.win_x += 1
        else:
            node.win_0 += 1
        if node.parents != None:
            self.improve_grade(node.parents, winner)
    def improve_draw(self, node):
        node.draw += 1
        if node.parents != None:
            self.improve_draw(node.parents)


b = [1, 2, 3, 4, 5, 6, 7, 8, 9]
valid_move = [0, 1, 2, 3, 4, 5, 6, 7, 8]
board = Board(b, valid_move)
root = Node(board)
tree = Tree(root)
tree.generate_tree(root)
print(f'''
      {root.win_x}
      {root.win_0}
      {root.draw}''')
d = root


while True:
    temp = None
    while True:
        chs = input('Игрок, выбери клетку: ')
        if chs.isdigit():
            temp = int(chs)-1
            break
    for i in d.child:
        if i.move == temp:
            d = i
            break
    d.board.showboard()
    if d.board.winner != None:
        print('Победитель:', d.board.winner)
        break
    
    m = 0
    u = None
    for i in d.child:
        if i.win_0/(i.win_0 + i.win_x + i.draw) > i.win_x/(i.win_0 + i.win_x + i.draw): 
            if i.win_0/(i.win_0 + i.win_x + i.draw) > m:
                m = i.win_0/(i.win_0 + i.win_x + i.draw)
                u = i
        if i.win_x/(i.win_0 + i.win_x + i.draw) > i.win_0/(i.win_0 + i.win_x + i.draw):
            if i.win_x/(i.win_0 + i.win_x + i.draw) > m:
                m = i.win_x/(i.win_0 + i.win_x + i.draw)
                u = i
    d = u
    print('Компьютер сделал свой выбор.')
    d.board.showboard()
    if d.board.winner != None:
        print('Победитель:', d.board.winner)
        break
    
